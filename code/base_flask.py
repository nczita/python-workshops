# -*- coding: utf-8 -*-

from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
@app.route('/<path:path>')
def hello(path=None):
    path = path or ""
    return render_template('index.html', path=path)

if __name__ == "__main__":
    app.run()