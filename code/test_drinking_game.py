import drinking_game
import random

def drink_game(mike_max, joe_max):
    drinkers_max = max([mike_max, joe_max])
    if mike_max < 1 or joe_max < 1:
        return "Non-drinkers can't play"
    mike = 0
    joe = 0
    winner = ""
    for i in range(1, drinkers_max+1):
        if i % 2 == 0:
            joe += i
            if joe > joe_max:
                winner = "Mike"
                break
        else:
            mike += i
            if mike > mike_max:
                winner = "Joe"
                break
    return winner

for game in [drink_game, drinking_game.game]:
    print(game)
    assert game(0, 1) == "Non-drinkers can't play"
    assert game(1, 0) == "Non-drinkers can't play"
    # assert game(-1, 0) == "Non-drinkers can't play"
    # assert game(-1, -2) == "Non-drinkers can't play"
    assert game(3, 2) == "Joe"
    assert game(2, 3) == "Joe"
    assert game(10, 3) == "Mike"
    assert game(11, 12) == "Joe"
    assert game(144, 156) == "Joe"
    assert game(33, 34) == "Joe"

for i in range(100):
    a = random.randint(0, i)
    b = random.randint(0, i)
    try:
        assert drink_game(a, b) == drinking_game.game(a, b)
    except AssertionError as e:
        print('Error', a, b, drink_game(a, b), drinking_game.game(a, b))