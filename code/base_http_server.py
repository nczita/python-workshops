# -*- coding: utf-8 -*-

from http.server import HTTPServer, BaseHTTPRequestHandler
import logging


# From: https://wiki.python.org/moin/BaseHttpServer
class SimpleHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        """Respond to a GET request."""
        self.send_response(200, 'OK')
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(b"<html><head><title>Title goes here.</title></head>")
        self.wfile.write(b"<body><p>This is a test.</p>")
        # If someone went to "http://something.somewhere.net/foo/bar/",
        # then self.path equals "/foo/bar/".
        self.wfile.write(bytes("<p>You accessed path: %s</p>" % self.path, 'utf-8'))
        self.wfile.write(b"</body></html>")

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    handler_classes = [BaseHTTPRequestHandler, SimpleHandler]
    for handler_class in handler_classes:
        try:
            logging.info("Starting server with handler class: %s" % handler_class)
            run(server_class=HTTPServer, handler_class=handler_class)
        except KeyboardInterrupt as e:
            logging.exception("Server exited by keyboard interuption")
