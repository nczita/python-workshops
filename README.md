Python workshops
================

1. [Lesson 0 - Non Python related basics](./00_lesson/README.md)
1. [Lesson 1 - Python related basics](./01_lesson/README.md)
1. [Lesson 2 - Reading and writing files and json](./02_lesson/README.md)
1. [Lesson 3 - HTTP and REST](./03_lesson/README.md)
1. [Lesson 4 - HTTP Servers](./04_lesson/README.md)
1. [Lesson 5 - SQL](./05_lesson/README.md)
1. [Lesson 6 - Deploy](./06_lesson/README.md)
1. [Lesson 7 - Exam](./07_lesson/README.md)
