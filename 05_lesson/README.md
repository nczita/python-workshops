SQL
===

SQL
---

```python

import sqlite3
conn = sqlite3.connect("db.sqlite")

c = conn.cursor()
c.execute("CREATE TABLE users (name VARCHAR)")

conn.commit()

c = conn.cursor()
c.execute("INSERT INTO users (name) VALUES ('Lukasz')")

conn.commit()

c.execute("SELECT * FROM users")
c.fetchall()

c.close()
conn.close()
```


SqlAlchemy
----------

```python 
import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)

engine = create_engine('sqlite:///sqlalchemy.db')

Base.metadata.create_all(engine)
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)

session = DBSession()

new_user = User(name='new user')
session.add(new_user)
session.commit()

user = session.query(User).first()

print(user)
print(user.name)
```

Migrations
----------

TBD