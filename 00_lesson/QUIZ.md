Basic workflow
==============

Create branch
```
git checkout -b pytut
```

Changes
```
echo "Hello Word" >> README.md
```

Commit
```
git commit -am "Add Hello Word NEW BRANCH"
```

Push to origin
```
git push --set-upstream origin pytut
```


