Lesson 0 - Non Python related basics
====================================

1. Bash, console, terminal
    - [Appendix A: Command Line Crash Course](https://learnpythonthehardway.org/python3/appendixa.html)
1. git
    - [Getting Started](https://www.atlassian.com/git/tutorials/setting-up-a-repository)
1. GitHub
    - [Hello world](https://guides.github.com/activities/hello-world/)
1. Bitbucket
    - [Learn Git with Bitbucket Cloud](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)
1. SourceTree
    - [SourceTree](https://www.sourcetreeapp.com/)
1. Visual Studio Code
    - [VS Code](https://code.visualstudio.com/)


Bash, console, terminal
-----------------------

1. pwd
1. cd
1. mkdir
1. rmdir
1. rm
1. touch
1. cat
1. less
1. pbcopy / pbpaste

git
---

1. git init
1. git add
1. git rm
1. git mv
1. git push
1. git pull
1. git checkout
1. git commit -m "Initial commit"

GitHub
------

1. Create PR
1. Review PR
1. Close PR

Bitbucket
---------

1. Pipeline

SourceTree
----------

1. Setup
1. Kdiff3

Visual Studio Code
------------------

1. Setup
1. Plugins


Meeting 22.05.2018
------------------

Entry point

- none

Exit point

- basic bash commands
- basic git commands
- setup bitbucket repository and SSH access keys.

Meeting 29.05.2018
------------------

Entry point

- basic bash commands
- basic git commands
- account at bitbucket
- repository at bitbucket
- ssh keys set up

Exit point

- TBD

