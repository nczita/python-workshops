Lesson 2 - Reading and writing files and json
=============================================

1. Reading and writing files
1. JSON


Reading and writing files
-------------------------

```python
with open('README.md') as f:
    print(f.read())
```

```python
with open('README.md') as f:
    for line in f:
        print(line)
```

```python
with open('NOT_A_README.md', 'w') as f:
    f.write("Hello world!")
```

```python
with open('NOT_A_README.md', 'w') as f:
    print("Another hello world!", file=f)
```

```python
with open('NOT_A_README.md', 'a') as f:
    f.write("Where is the first")
```

```python
f = open('NOT_A_README.md', 'a')
f.write("Without context")
f.close()
```

```python
f = open('README.md', 'r')
print(f.tell())
print(f.read())
print('-'*80)
print(f.tell())
print(f.read())
print('-'*80)
print(f.seek(0))
print(f.tell())
print(f.read())
print('-'*80)
print('-'*80)
print(f.seek(20))
print(f.tell())
print(f.read())
print('-'*80)
f.close()

```

```python
with open('README.md') as f:
    f.readline()
    f.read(10)
```

```python
with open('NOT_A_README.md', 'w') as f:
    f.writelines(['SOME\n', 'Lines'])
    f.write('NOT \nA LINE')
```

JSON
----

```python
import json

data = {'a' : 10, 'b': 20}
print(json.dumps(data))
with open('data.json', 'w') as f:
    json.dump(data, f)
```

```python
import json

with open('data.json', 'r') as f:
    data = json.load(f)

print(data['a'])
```

```python
import json

with open('data.json', 'r') as f:
    data_str = f.read()
    data = json.loads(data_str)

print(data['a'])
```

