HTTP Servers
============

Base HTTP server
----------------

In base HTTP server everything have to be done by developer.
You need to specify, to match request, what verb of HTTP protocol you want to respond to ex. do_GET, do_POST, do_DELETE.
Developer need to explicitly return code by `send_response(code, message=None)`.
Send headers by `send_header(keyword, value)` and also specify that we send them all by `end_headers()`.
Body of response is treated as file `self.wfile` that developer need to fill as usual with method `self.wfile.write`.

As you could see there is strict analogy with 3 part HTTP request/response format.


```python
# -*- coding: utf-8 -*-

from http.server import HTTPServer, BaseHTTPRequestHandler
import logging


# From: https://wiki.python.org/moin/BaseHttpServer
class SimpleHandler(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'
    def do_GET(self):
        """Respond to a GET request."""
        # send response - code and message
        self.send_response(200, 'OK')
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(b"<html><head><title>Title goes here.</title></head>")
        self.wfile.write(b"<body><p>This is a test.</p>")
        # If someone went to "http://something.somewhere.net/foo/bar/",
        # then self.path equals "/foo/bar/".
        self.wfile.write(bytes("<p>You accessed path: %s</p>" % self.path, 'utf-8'))
        self.wfile.write(b"</body></html>")

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    for handler_class in [BaseHTTPRequestHandler, SimpleHandler]:
        try:
            logging.info("Starting server with handler class: %s" % handler_class)
            run(server_class=HTTPServer, handler_class=handler_class)
        except KeyboardInterrupt as e:
            logging.exception("Server exited by keyboard interuption")

```

Flask
-----

Heh this is much more simpler.
Just few annotations and template.
Flask even figure out what status and header need to be in this situation.


```python
# -*- coding: utf-8 -*-

from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
@app.route('/<path:path>')
def hello(path=None):
    path = path or ""
    return render_template('index.html', path=path)

if __name__ == "__main__":
    app.run()
```

Gunicorn
--------

`Gunicorn 'Green Unicorn' is a Python WSGI HTTP Server for UNIX. It's a pre-fork worker model. The Gunicorn server is broadly compatible with various web frameworks, simply implemented, light on server resources, and fairly speedy.`

So lets use it:

```bash
$ gunicorn -w 4 base_flask:app
```

