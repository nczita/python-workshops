# -*- coding: utf-8 -*-
import requests

def get_list_of_posts():
    response = requests.get("https://jsonplaceholder.typicode.com/posts")
    print(response.status_code)
    print('*'*80)
    print(response.headers)
    print('*'*80)
    print(response.content[:500])
    print('*'*80)
    print(response.json())
    return response

if __name__ == "__main__":
    get_list_of_posts()
