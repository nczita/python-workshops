HTTP and REST
=============

HTTP
----

`HTTP is an application layer protocol designed within the framework of the Internet protocol suite. Its definition presumes an uderlying and reliable transport layer protocol, and Transmission Control Protocol (TCP) is commonly used.` - wikipedia.

HTTP is one of most commonly used communication protocol. It is used by servers and browser to give users rich web pages.
But also it is used to communication between services. Today we stick to rather this part.
We try to find ourselves in this communication as client.

But first let's review some basics, please take look on those very simple examples:
```http
GET /?say=Hi&to=Mom HTTP/1.1
Host: foo.com
```

```http
POST / HTTP/1.1
Host: foo.com
Content-Type: application/x-www-form-urlencoded
Content-Length: 13

say=Hi&to=Mom
```

So after reviewing those, we could distinguish essential parts:

1. A request line has three parts divided by space:
    * starts with verb like: GET, POST, PUT etc.
    * followed by resource path: / , '/images/image.png', '/?say=Hi&to=Mom'
    * ends with HTTP protocol version specification
2. Header fields is a list, each item in newline, of pairs: header fields name and after colon value of header field, like: "Host: foo.com", "Content-Type: application/json", "Accept-Language: en".
3. Optional part, empty line and body of request. Body could be any "printable" content, image, html, json, form-data etc.

Note how it is constructed the resource path `/?say=Hi&to=Mom`. The part after `?` is called query string, query string is pairs (again!) of keys and values divided by `&`, the key from value is separated by `=`. We will talk about query string more in server part of this tutorial, for now please just treat query string almost the same as body - you probably already see that those are the same.

Server response is very similar, let's take a look:
```http
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8
Content-Length: 138
Server: Apache/1.3.3.7 (Unix) (Red-Hat/Linux)

{"message":"Hello World!"}
```

You could see that here we also could distinguish three parts:

1. Status line also have three parts divided by space:
    * HTTP protocol version
    * response status code, three digits are defined by standard
    * reason message, usually correlated with status code
2. Header fields ... (same as in request)
3. Optional part, ... (same as in request)

So commonly seen status codes:

* 1XX - Informational responses
* 2XX - Success
    * 200 OK
    * 201 Created
* 3XX - Redirection
    * 301 Moved Permanently
    * 304 Not Modified
* 4XX - Client errors
    * 400 Bad Request
    * 401 Unauthorized
    * 404 Not Found
* 5XX - Server errors
    * 500 Internal Server Error
    * 504 Gateway Timeout

Please open "Developers tools" in Chrome (Cmd-Alt-I) at network tab and try to open several pages, like:

- https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
- https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol12354
- https://app.airhelp.com/

REST
----

REST `is an architectural style` used commonly to exchange information between services, based on HTTP.
`REST-compliant web services allow the requesting systems to access and manipulate textual representations of web resources by using a uniform and predefined set of stateless operations.` - wikipedia .

Usually as today, REST is often paired with JSON. JSON represents data of our systems, and REST offers actions to operate on that data. For example pattern like CRUD:

- C - create, HTTP POST
- R - read, HTTP GET
- U - update, HTTP PUT
- D - delete, HTTP DELETE

So imagine that we have available simple blog like API (actually don't imagine - just use https://jsonplaceholder.typicode.com/). Let's try to make some basics actions. Oh wait ;) Let's install needed tools for it.

Install great python library for REST communication called `requests`, so:

Always try to work inside some virtual environment:
```bash
$ source venv/bin/activate
```

Install `requests`:
```bash
$ pip install requests
```

... that is overcomplicated.

So basics, let's see list of posts:

```python
# -*- coding: utf-8 -*-
import requests

def get_list_of_posts():
    response = requests.get("https://jsonplaceholder.typicode.com/posts")
    print(response.status_code)
    print('*'*80)
    print(response.headers)
    print('*'*80)
    print(response.content[:500])
    print('*'*80)
    print(response.json())
    return response

if __name__ == "__main__":
    get_list_of_posts()

```