`print("Hello world")`

```
a = 10
b=20
```

`a + b`

`a < b`

`l = [10, 20, "some"]`

`l[1]`

`l[0:2]`

`l[-1]`

`range(10)`

```
if a < b:
    print("b is grater")
else:
    print("a is grater")
```

```
for i in range(3):
    print("Some i:", i)
```

```
while True:
    print("Break")
    break
```

```
def sum(a, b):
    return a+b
```

```
a = {'a' : 10, 'b': 20}
def max(d):
    m = 0
    mk = ""
    for k, v in d.items():
        if m < v:
            m = v
            mk = k
    return mk
max(a)
```
